import { faker } from '@faker-js/faker';
const email = faker.internet.email()
const password = faker.internet.password()
const randomData = faker.internet.userName()

describe('Sign In Website Katrecs : Kata Recruiter', () => {
  before(() => {
    cy.visit('https://katrecs.katalabs.io/register');
    cy.get('input[name="email"]').type(`${email}`);
    cy.get('input[name="password"]').type(`${password}`);
    cy.get('input[name="passwordConfirmation"]').type(`${password}`);
    cy.get('button[id="rTalent"]').click();
    cy.get('button[type="submit"]').click();
  });
  beforeEach(() => {
    cy.visit('https://katrecs.katalabs.io/login');
  });

  it('When the user successfully registers, then should be success login', () => {
    cy.get('input[name="email"]').type(`${email}`);
    cy.get('input[name="password"]').type(`${password}`);
    cy.get('button[type="submit"]').click();
  });

  it('When the user login using the wrong password should fail to login', () => {
    cy.get('input[name="email"]').type(`${email}`);
    cy.get('input[name="password"]').type(`${randomData}`);
    cy.get('button[type="submit"]').click();
    cy.contains("Email and/or password doesn't match");
  });

  it('When the user login using the wrong email should fail to login', () => {
    cy.get('input[name="email"]').type('test@mail.com');
    cy.get('input[name="password"]').type(`${password}`);
    cy.get('button[type="submit"]').click();
    cy.contains("Email and/or password doesn't match");
  });

  it('When the user login using the one data mandatory is empty should fail to login', () => {
    cy.get('input[name="email"]').type(`${email}`);
    cy.get('input[name="password"]');
    cy.get('button[type="submit"]').should('be.disabled');
  });
});