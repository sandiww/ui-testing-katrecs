import { faker } from '@faker-js/faker';
const email = faker.internet.email()
const password = faker.internet.password()
const randomData = faker.internet.userName();

describe('Sign Up Website Katrecs : Kata Recruiter', () => {
  beforeEach(() => {
    cy.visit('https://katrecs.katalabs.io/register');
  });

  it('When user registration using valid data should be success', () => {
    cy.get('input[name="email"]').type(`${email}`);
    cy.get('input[name="password"]').type(`${password}`);
    cy.get('input[name="passwordConfirmation"]').type(`${password}`);
    cy.get('button[id="rTalent"]').click();
    cy.get('button[type="submit"]').click();
    cy.contains(`${email}`);
  });

  it('When user registration using one mandatory data is empty should fail', () => {
    cy.get('input[name="email"]').type(`${email}`);
    cy.get('input[name="password"]').type(`${password}`);
    cy.get('input[name="passwordConfirmation"]').type(`${password}`);
    cy.get('button[id="rTalent"]')
    cy.get('button[type="submit"]').should('be.disabled');
  });

  it('When the user registration using the format email non compliant should fail', () => {
    cy.get('input[name="email"]').type(`${randomData}`);
    cy.get('input[name="password"]').type(`${password}`);
    cy.get('input[name="passwordConfirmation"]').type(`${password}`);
    cy.get('button[id="rTalent"]').click();
    cy.get('button[type="submit"]').should('be.disabled');
  });

  it('When the user registration using confirm password not the same as the password in the registered should fail', () => {
    cy.get('input[name="email"]').type(`${email}`);
    cy.get('input[name="password"]').type(`${password}`);
    cy.get('input[name="passwordConfirmation"]').type(`${randomData}`);
    cy.get('button[id="rTalent"]').click();
    cy.get('button[type="submit"]').should('be.disabled');
  });
});